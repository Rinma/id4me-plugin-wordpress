<?php

use Id4me\RP\Service as ID4meService;

/**
 * Class ID4me_Client
 */
class ID4me_Client extends ID4me_Env {

	/**
	 * @var ID4meService
	 */
	private $id4me_service;

	/**
	 * ID4me_Client constructor.
	 *
	 * @param ID4meService
	 */
	public function __construct( $id4me_service = null ) {

		if ( ! $id4me_service instanceof ID4meService ) {
			$this->set_id4me_service( new ID4meService( new ID4me_Http_Client() ) );
		} else {
			$this->set_id4me_service( $id4me_service );
		}
	}

	/**
	 * Connect to the authority
	 * @action init
	 */
	public function connect() {

		if ( $this->is_action( 'connect' ) ) {

			$identifier = $this->get_param( 'id4me_identifier' );
			$redirect_url = $this->get_redirect_url();

			// 1. Discover (retrieve DNS TXT record)
			$authority_hostname = $this->discover_authority( $identifier );

			if ( ! $authority_hostname ) {
				$this->log_error( __( 'Unknown Authority for this identifier', 'id4me' ) );
				return;
			}

			// 2. Retrieve Authority configuration and credentials
			$authority = $this->retrieve_authority_data( $authority_hostname, $redirect_url );

			if ( ( ! $authority instanceof ID4me_WP_Authority ) || empty( $authority->get_hostname() ) ) {
				$this->log_error( __( 'Impossible to retrieve Authority configuration', 'id4me' ) );
				return;
			}

			// 3. Authority successfully loaded/registered -> Redirect to it to log in
			$this->redirect_to_authority( $authority, $identifier, $redirect_url );

			// If no redirection happened and no error is returned, something wrong happened
			if ( empty( $this->get_errors() ) ) {
				$this->log_error( __( 'An unexpected error has occurred, unable to redirect to Authority', 'id4me' ) );
			}
		}
	}

	/**
	 * Log in user after successful authentication through the authority
	 * @action authenticate
	 *
	 * @param  WP_User | null $user
	 * @return WP_User
	 */
	public function auth( $user ) {

		// Do nothing if a user is already logged in
		if ( $user instanceof WP_User ) {
			return $user;
		}

		// Are we trying to log in with ID4me?
		if ( $this->is_action( 'auth' ) ) {

			$code = $this->get_param( 'code' );
			$state = $this->get_param( 'state' );

			// 1. Get the ID4me identifier that comes back after possible authentication
			$identifier = $this->retrieve_identifier_from_state( $state );

			// 2. Discover again to know the authority
			$authority_hostname = $this->discover_authority( $identifier );

			if ( ! $authority_hostname ) {
				$this->log_error( __( 'Unknown Authority for this identifier', 'id4me' ) );
				return null;
			}

			// 3. Validate user token code with the authority
			$access_data = $this->authorize_user( $authority_hostname, $code );

			// 4. If authorization successful, retrieve corresponding WordPress user
			if ( is_array( $access_data ) && array_key_exists( 'identifier', $access_data ) ) {
				$user = $this->retrieve_wp_user( $access_data['identifier'] );
			} else {
				$user = null;
			}

			// We are in the authentication hook and just need to return a valid WP_User to log in
			if ( $user instanceof WP_User ) {
				return $user;
			} else {
				$this->log_error( __( 'Access denied for user ' . $identifier, 'id4me' ) );
			}
		}

		return null;
	}

	/**
	 * Call discover() from the ID4me Service to retrieve DNS TXT records from identifier domain
	 *
	 * @param  string $identifier
	 * @return boolean | string
	 */
	public function discover_authority( $identifier ) {

		if ( empty( $identifier ) ) {
			$this->log_error( __( 'Please enter a valid identifier', 'id4me' ) );
			return false;
		}

		try {
			return $this->id4me_service->discover( $identifier );

		} catch ( Exception $exception ) {
			$this->log_error( __( 'No DNS configuration found for identifier', 'id4me' ) );
			return false;
		}
	}

	/**
	 * Retrieves the ID4me credentials and data for the given authority:
	 *    - If already registered, load data from DB
	 *    - If not, register the authority and persist data
	 *
	 * @param  string $authority_hostname
	 * @param  string $redirect_url
	 * @return boolean | ID4me_WP_Authority
	 */
	public function retrieve_authority_data( $authority_hostname, $redirect_url ) {

		$authority = new ID4me_WP_Authority();

		// Load info from wp_authority DB table
		if ( ! $authority->load_by_hostname( $authority_hostname ) || $authority->has_expired() ) {

			// Accept localhost domains in dev mode
			if ( $this->get_env() === 'dev' ) {
				$application_type = 'native';
			} else {
				$application_type = 'web';
			}

			// OpenID config call
			$configuration = $this->id4me_service->getOpenIdConfig(
				$authority_hostname
			);

			// Registration
			try {
				$client = $this->id4me_service->register(
					$configuration,
					$this->get_client_name(),
					$redirect_url,
					$application_type
				);

			} catch ( Exception $exception ) {
				$this->log_error( __( 'Registration request returned an error', 'id4me' ) );
				return false;
			}

			// Save client/authority data in the authority DB table
			$authority->set_hostname( $authority_hostname );
			$authority->set_client_id( $client->getClientId() );
			$authority->set_client_secret( $client->getClientSecret() );
			$authority->set_configuration( $configuration->getData() );

			// Set expiration date for the entry
			$expiration_time = (int) $client->getClientExpirationTime();

			if ( $client->getClientExpirationTime() !== 0 ) {
				$authority->set_expired( date( 'Y-m-d H:i:s', $expiration_time ) );
			}

			if ( ! $authority->save() ) {
				$this->log_error( __( 'Unable to register client and authority data in the database', 'id4me' ) );
				return false;
			}
		}

		return $authority;
	}

	/**
	 * Redirect to authority login URL with all necessary callback parameters
	 *
	 * @param ID4me_WP_Authority $authority
	 * @param string             $identifier
	 * @param string             $redirect_url
	 */
	public function redirect_to_authority( $authority, $identifier, $redirect_url ) {

		if ( $authority instanceof ID4me_WP_Authority && ! empty( $authority->get_hostname() ) ) {

			// Build the state we need when we come back to authorize the user
			$identifier_hash_key = uniqid( '', true );

			// Memorize identifier hash for 10 min, so that we know which user is coming back to log in
			set_transient( 'id4me' . $identifier_hash_key, $identifier, 600 );

			// Build authority login URL (identifier hash is used as state)
			$login_link = $this->id4me_service->getAuthorizationUrl(
				$this->get_config_object( $authority->get_configuration() ),
				$authority->get_client_id(),
				$identifier,
				$redirect_url,
				$identifier_hash_key
			);

			if ( empty( $login_link ) ) {
				$this->log_error( __( 'Request for authorization URL returned empty response', 'id4me' ) );

			} else {
				wp_redirect( $login_link );
			}
		}
	}

	/**
	 * Retrieve the identifier of the user who wants to log in
	 *
	 * @param  string $state
	 * @return boolean | string
	 */
	public function retrieve_identifier_from_state( $state ) {

		// Does state exists?
		if ( empty( $state ) ) {
			$this->log_error( __( 'No valid state returned from authority', 'id4me' ) );
			return false;
		}

		// Retrieve the identifier corresponding to this hash
		$identifier = get_transient( 'id4me' . $state );

		if ( empty( $identifier ) ) {
			$this->log_error( __( 'Unknown identifier given', 'id4me' ) );
			return false;
		}

		// Delete cache once it has been used
		delete_transient( 'id4me' . $state );

		return $identifier;
	}

	/**
	 * Authorization process: validate user code and return the identifier of the authorized user
	 *
	 * @param  string $authority_hostname
	 * @param  string $code
	 * @return array | boolean
	 */
	public function authorize_user( $authority_hostname, $code ) {

		$authority = new ID4me_WP_Authority();

		// Retrieve authority data in the database
		if ( ! $authority->load_by_hostname( $authority_hostname ) ) {
			$this->log_error( __( 'No authority is registered for this hostname', 'id4me' ) );
			return false;
		}

		// Check if authority client credentials are still valid
		if ( $authority->has_expired() ) {
			$this->log_error( __( 'Authority client credentials have expired', 'id4me' ) );
			return false;
		}

		// Call authorization service to ensure user is logged in with ID4me
		try {
			$access_data = $this->id4me_service->authorize(
				$this->get_config_object( $authority->get_configuration() ),
				$code,
				$this->get_redirect_url(),
				$authority->get_client_id(),
				$authority->get_client_secret()
			);

		} catch ( Exception $exception ) {
			$this->log_error( __( 'Authorization request from ' . $authority->get_hostname() . ' returned an error', 'id4me' ) );
			return false;
		}

		return $access_data;
	}

	/**
	 * Select existing WP_User according to the given identifier, registered as metadata
	 *
	 * @param $identifier
	 * @return bool|WP_User
	 */
	public function retrieve_wp_user( $identifier ) {

		try {
			$user = new ID4me_User( $identifier );
			return $user->get_wp_user();

		} catch ( Exception $exception ) {
			$this->log_error( $exception->getMessage() );
			return false;
		}
	}

	/**
	 * Check if we are in the ID4me pop-up ("connect" step)
	 *
	 * @param  string $action
	 * @return boolean
	 */
	public function is_action( $action ) {
		return ! empty( $_GET['id4me_action'] )
				&& $_GET['id4me_action'] == $action;
	}

	/**
	 * Generates the OpenID configuration object for the ID4me service
	 * (uses a utility function from the service)
	 *
	 * @param  string $json_configuration
	 * @return \Id4me\RP\Model\OpenIdConfig
	 */
	public function get_config_object( $json_configuration ) {

		if ( ! empty( $json_configuration ) ) {
			return $this->id4me_service->createOpenIdConfigFromJson( $json_configuration );
		}

		return null;
	}

	/**
	 * Build the URL where the user is redirected after login on the authority page,
	 * then use as safety check by the authorization process
	 *
	 * @return string
	 */
	public function get_redirect_url() {

		$redirect_url = add_query_arg( array(
			'id4me_action' => 'auth',
		), wp_login_url() );

		return $redirect_url;
	}

	/**
	 * Return client name
	 * (fixed, client domain (my current domain))
	 *
	 * @return string
	 */
	public function get_client_name() {
		return wp_parse_url( home_url(), PHP_URL_HOST );
	}

	/**
	 * Get the given as parameter in the URL
	 *    - identifier to log in (ID4me username)
	 *    - redirect link to come back after authentication through the authority
	 *    - user token after authentication through the authority
	 *
	 * @param  string $param
	 * @return boolean | string
	 */
	public function get_param( $param ) {

		if ( ! in_array( $param, array( 'id4me_identifier', 'code', 'state' ) ) ) {
			return false;
		}
		if ( ! empty( $_GET[ $param ] ) ) {

			// sanitize_text_field() strips all the stuff we don't want in an URL param
			return sanitize_text_field( $_GET[ $param ] );
		}

		return false;
	}

	/**
	 * Save the ID4me service instance
	 *
	 * @param ID4meService $id4me_service
	 */
	public function set_id4me_service( ID4meService $id4me_service ) {

		if ( $id4me_service instanceof ID4meService ) {
			$this->id4me_service = $id4me_service;
		}
	}
}
