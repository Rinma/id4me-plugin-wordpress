<?php
/**
 * Plugin Name:  ID4me
 * Plugin URI:   https://wordpress.org/plugins/id4me/
 * Description:  One ID for everything. Log in into your WordPress with your domain, through any service you like!
 * Version:      1.0.1
 * License:      MIT
 * Author:       1&1 IONOS
 * Author URI:   https://www.ionos.com
 * Requires PHP: 7.1
 * Text Domain:  id4me
 */

// Require external libraries
require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

// Require dependencies/instances
require_once 'includes/class-id4me-wp-authority.php';
require_once 'includes/class-id4me-user.php';
require_once 'includes/class-id4me-env.php';
require_once 'includes/class-id4me-login.php';
require_once 'includes/class-id4me-client.php';
require_once 'includes/class-id4me-http-client.php';

// Installation hooks
register_activation_hook( __FILE__, 'id4me_activate' );
register_uninstall_hook( __FILE__, 'id4me_uninstall' );

// Loading hooks
add_action( 'plugins_loaded', 'id4me_init' );
add_action( 'plugins_loaded', 'id4me_cron_init' );
add_action( 'init', 'id4me_load_textdomain' );

// Register clean-up cron jobs
add_action( 'id4me_cron_cleanup', 'id4me_delete_expired_transients' );

/**
 * Init
 */
function id4me_init() {

	// ID4me hooks on login page
	$id4me_login = new ID4me_Login();
	add_filter( 'login_footer', array( $id4me_login, 'login_form' ) );
	add_action( 'login_form', array( $id4me_login, 'login_link' ) );
	add_filter( 'login_body_class', array( $id4me_login, 'login_body_class' ) );
	add_filter( 'login_errors', array( $id4me_login, 'login_body_class' ) );
	add_action( 'login_enqueue_scripts', array( $id4me_login, 'enqueue_scripts' ) );

	// ID4me hooks to handle authority client
	$id4me_client = new ID4me_Client();
	add_action( 'init', array( $id4me_client, 'connect' ) );
	add_filter( 'authenticate', array( $id4me_client, 'auth' ), 1 );

	// ID4me hooks to handle user <-> identifier association
	add_action( 'show_user_profile', array( 'ID4me_User', 'create_profile_fields' ) );
	add_action( 'edit_user_profile', array( 'ID4me_User', 'create_profile_fields' ) );
	add_action( 'personal_options_update', array( 'ID4me_User', 'save_profile_fields' ) );
	add_action( 'edit_user_profile_update', array( 'ID4me_User', 'save_profile_fields' ) );
}

/**
 * Init/Schedule Cron
 */
function id4me_cron_init() {

	if ( ! wp_next_scheduled( 'id4me_cron_cleanup' ) ) {
		wp_schedule_event( time(), 'daily', 'id4me_cron_cleanup' );
	}
}

/**
 * Init translations
 */
function id4me_load_textdomain() {

	load_plugin_textdomain(
		'id4me',
		false,
		basename( dirname( __FILE__ ) ) . '/languages'
	);
}

/**
 * Activation
 */
function id4me_activate() {

	// Check if ext-openssl is loaded
	if ( ! extension_loaded( 'openssl' ) ) {
		deactivate_plugins( plugin_basename( __FILE__ ) );

		wp_die( esc_html__(
			'The requested PHP extension "openssl" is missing from your system, please contact your hosting provider.', 'id4me'
		) );
	}

	// Create authority table
	ID4me_WP_Authority::create_table();
}

/**
 * Uninstall (clean-up)
 */
function id4me_uninstall() {

	// Remove authority table
	ID4me_WP_Authority::delete_table();
}

/**
 * Clean up old transients that may remain from previous login attempts (= transients with "id4me" prefix)
 * Fix/Alternative for the WordPress function that deletes ALL
 * @see delete_expired_transients()
 *
 * @param string  $prefix
 */
function id4me_delete_expired_transients( $prefix = 'id4me' ) {
	global $wpdb;

	$transient_prefix_end_position = strlen( $prefix ) + 12;
	$transient_timeout_prefix_end_position = strlen( $prefix ) + 17;

	$wpdb->query( $wpdb->prepare(
		"DELETE a, b FROM {$wpdb->options} a, {$wpdb->options} b
		WHERE a.option_name LIKE %s
		AND a.option_name NOT LIKE %s
		AND b.option_name = CONCAT( %s, SUBSTRING( a.option_name, %d ) )
		AND b.option_value < %d",
		$wpdb->esc_like( '_transient_' . $prefix ) . '%',
		$wpdb->esc_like( '_transient_timeout_' . $prefix ) . '%',
		'_transient_timeout_' . esc_sql( $prefix ),
		$transient_prefix_end_position,
		time()
	) );

	if ( ! is_multisite() ) {

		$wpdb->query( $wpdb->prepare(
			"DELETE a, b FROM {$wpdb->options} a, {$wpdb->options} b
			WHERE a.option_name LIKE %s
			AND a.option_name NOT LIKE %s
			AND b.option_name = CONCAT( %s, SUBSTRING( a.option_name, %d ) )
			AND b.option_value < %d",
			$wpdb->esc_like( '_site_transient_' . $prefix ) . '%',
			$wpdb->esc_like( '_site_transient_timeout_' . $prefix ) . '%',
			'_transient_timeout_' . esc_sql( $prefix ),
			$transient_timeout_prefix_end_position,
			time()
		) );

	} elseif ( is_multisite() && is_main_site() && is_main_network() ) {

		$wpdb->query( $wpdb->prepare(
			"DELETE a, b FROM {$wpdb->sitemeta} a, {$wpdb->sitemeta} b
			WHERE a.meta_key LIKE %s
			AND a.meta_key NOT LIKE %s
			AND b.meta_key = CONCAT( %s, SUBSTRING( a.meta_key, %d ) )
			AND b.meta_value < %d",
			$wpdb->esc_like( '_site_transient_' . $prefix ) . '%',
			$wpdb->esc_like( '_site_transient_timeout_' . $prefix ) . '%',
			'_transient_timeout_' . esc_sql( $prefix ),
			$transient_timeout_prefix_end_position,
			time()
		) );
	}
}
