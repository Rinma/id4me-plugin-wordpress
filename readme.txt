=== ID4me ===
Contributors: gdespoulain, herrfeldmann, kimalumalu, pfefferle, rinma, ionos
Tags: ID4me, login, domain
Requires at least: 5.0
Requires PHP: 7.1
Tested up to: 5.2
Stable tag: 1.0.1
License: MIT
License URI: https://opensource.org/licenses/MIT
Author: 1&1 IONOS

This plugin allows users to use their domain to sign in into WordPress, through any third party of their choice.
It uses the **ID4me protocol** and requires a domain that has been registered with ID4me.

== Description ==

[ID4me](https://id4me.org/) is a public, open, federated digital identity service that centralizes user access and user data into one
choosen platform, without the user being subsequently bound to said platform, i.e. with the possibility for the
user to be able to switch platform without losing any connection to the services where they connect.

== Frequently Asked Questions ==

= How does it work? =

The domain itself contains the information about which platform (any third party supporting the ID4me protocol)
it uses to log in its **DNS records**; it means, internally.

= How do I obtain an ID4me identifier? =

In order for any domain to become an ID4me username, the domain must be registered to an identity authority
through an identity agent.

Then, the resulting DNS record must be created with the identifying data (identity authority, identity agent) in order
for the identifier to work.

== Changelog ==

= 1.0.1 =

* Update doc with tested up to 5.2

= 1.0.0 =

* Add cron job to clean up ID4me expired transients
* Add custom HTTP client
* Fix datetime compatibility with MySQL 5.5
* Update library version to 1.0.0

= 0.1.1 =

* Implement WP.org feedback: use appropriate sanitize() instead of esc() for sanitizing input in user settings

= 0.1.0 =

* Initial

== Installation ==

Follow the normal instructions for [installing WordPress plugins](https://codex.wordpress.org/Managing_Plugins#Installing_Plugins).

= Automatic Plugin Installation (from WordPress.org) =

To add a WordPress Plugin using the [built-in plugin installer](https://codex.wordpress.org/Administration_Screens#Add_New_Plugins):

1. Go to [Plugins](https://codex.wordpress.org/Administration_Screens#Plugins) > [Add New](https://codex.wordpress.org/Plugins_Add_New_Screen).
2. Type "`id4me`" into the **Search Plugins** box.
3. Find the WordPress Plugin you wish to install.
    1. Click **Details** for more information about the Plugin and instructions you may wish to print or save to help setup the Plugin.
    1. Click **Install Now** to install the WordPress Plugin.
4. The resulting installation screen will list the installation as successful or note any problems during the install.
5. If successful, click **Activate Plugin** to activate it, or **Return to Plugin Installer** for further actions.

= Automatic Plugin Installation (as archive) =

You can also install a Plugin as archive (in the Zip Format) when you have a local copy (please note that this method
is **not supported for Must-use WordPress Plugins**):

1. Go to [Plugins](https://codex.wordpress.org/Administration_Screens#Plugins) > [Add New](https://codex.wordpress.org/Plugins_Add_New_Screen).
2. Click **Upload Plugin** to display the WordPress Plugin upload field.
3. Click **Choose File** to navigate your local file directory.
4. Select the WordPress Plugin Zip Archive you wish to upload and install.
5. Click **Install Now** to install the WordPress Plugin.
3. If successful, click **Activate Plugin** to activate it, or **Return to Plugin Installer** for further actions.

= Manual Plugin Installation =

There are a few cases when manually installing a WordPress Plugin is appropriate.

* If you wish to control the placement and the process of installing a WordPress Plugin.
* If your server does not permit automatic installation of a WordPress Plugin.
* If you want to try the [latest development version](https://gitlab.com/ID4me/id4me-plugin-wordpress).

Installation of a WordPress Plugin manually requires FTP familiarity and the awareness that you may put your site at risk if you install a WordPress Plugin incompatible with the current version or from an unreliable source.

Backup your site completely before proceeding.

To install a WordPress Plugin manually:

1. Download your WordPress Plugin to your desktop.
    * Download from [the WordPress directory](https://wordpress.org/plugins/id4me/)
    * Download from [GitHub](https://gitlab.com/ID4me/id4me-plugin-wordpress/releases)
2. If downloaded as a zip archive, extract the Plugin folder to your desktop.
3. With your FTP program, upload the Plugin folder to the `wp-content/plugins` folder in your WordPress directory online.
4. Go to [Plugins screen](https://codex.wordpress.org/Administration_Screens#Plugins) and find the newly uploaded Plugin in the list.
5. Click **Activate** to activate it.

= Configuration of users =

**Note:** The (current) **1.0.1** version only works with already registered WordPress users (who indicated their ID4me identifier
in the **ID4me identifier** profile field). Next versions will include a more complex user management.

To configure users to log in with ID4me:

1. Go to the user's [Profile Page](https://codex.wordpress.org/images/e/eb/profile.png).
2. Save your ID4me identifier in the 'Website' parameter.
3. Click **Update Profile**.

This identifier parameter can be updated anytime; the ID4me process will follow on the changes.