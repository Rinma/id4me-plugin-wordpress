<?php
require_once './includes/class-id4me-wp-authority.php';
require_once './includes/class-id4me-client.php';

class Test_ID4me_Client extends WP_UnitTestCase {

	/**
	 * @test ID4me_Client::discover_authority()
	 * @dataProvider provider_discover_authority
	 *
	 * @param string  $authority_hostname
	 * @param boolean $throws_exception
	 * @param string  $returned_authority_hostname
	 */
	public function test_discover_authority( $authority_hostname, $throws_exception, $returned_authority_hostname ) {

		$service = $this->_get_service_mock();

		if ( $throws_exception ) {
			$service->method( 'discover' )
			        ->willThrowException( new Exception( 'exception' ) );
		} else {
			$service->method( 'discover' )
			        ->willReturn( $authority_hostname );
		}

		$id4me_client = new ID4me_Client( $service );
		$authority_hostname = $id4me_client->discover_authority( 'wikipedia.org' );

		if ( empty( $authority_hostname ) || $throws_exception ) {
			$this->assertFalse( $returned_authority_hostname );

		} else {
			$this->assertEquals(
				$authority_hostname,
				$returned_authority_hostname
			);
		}
	}

	/**
	 * Data Provider for test_discover_authority()
	 *
	 * @return array
	 */
	public function provider_discover_authority() {

		return array(
			array(
				'dummy-authority.com',
				false,
				'dummy-authority.com'
			),
			array(
				null,
				false,
				false
			),
			array(
				'',
				false,
				false
			),
			array(
				false,
				false,
				false
			),
			array(
				'dummy-authority.com',
				true,
				false
			),
			array(
				'',
				true,
				false
			)
		);
	}

	/**
	 * @test ID4me_Client::retrieve_authority_data()
	 * @dataProvider provider_retrieve_authority_data
	 *
	 * @param boolean $register_throws_exception
	 */
	public function test_retrieve_authority_data( $register_throws_exception ) {

		ID4me_WP_Authority::create_table();

		$service = $this->_get_service_mock();

		$service->method( 'getOpenIdConfig' )
		        ->willReturn( $this->_get_dummy_configuration() );

		// First call: should register
		if ( $register_throws_exception ) {
			$service->expects( $this->once() )
			        ->method( 'register' )
			        ->willThrowException( new Exception( 'exception' ) );
		} else {
			$service->expects( $this->once() )
			        ->method( 'register' )
			        ->willReturn( $this->_get_dummy_client() );
		}

		$id4me_client = new ID4me_Client( $service );
		$authority = $id4me_client->retrieve_authority_data( 'dummy-authority.com', 'dummy-redirect.com' );

		if ( $register_throws_exception ) {
			$this->assertFalse( $authority );

		} else {
			$this->assertInstanceOf( 'ID4me_WP_Authority', $authority );
			unset( $authority );

			// Second call: should load directly without registering
			$authority = $id4me_client->retrieve_authority_data( 'dummy-authority.com', 'dummy-redirect.com' );

			$service->expects( $this->never() )
			        ->method( 'register' );

			$this->assertInstanceOf( 'ID4me_WP_Authority', $authority );
		}

		ID4me_WP_Authority::delete_table();
	}

	/**
	 * Data Provider for test_retrieve_authority_data()
	 *
	 * @return array
	 */
	public function provider_retrieve_authority_data() {

		return array(
			array(
				false
			),
			array(
				true
			)
		);
	}

	/**
	 * @test ID4me_Client::redirect_to_authority()
	 * @dataProvider provider_redirect_to_authority
	 *
	 * @param ID4me_WP_Authority $authority
	 * @param string             $authorization_url
	 * @param boolean            $redirected
	 */
	public function test_redirect_to_authority( $authority, $authorization_url, $redirected ) {
		$redirection = null;

		$service = $this->_get_service_mock();

		$service->method( 'createOpenIdConfigFromJson' )
		        ->willReturn( $this->_get_dummy_configuration() );

		$service->method( 'getAuthorizationUrl' )
		        ->willReturn( $authorization_url );

		// Avoid real WordPress redirection when calling wp_redirect()
		add_filter( 'wp_redirect', function ( $location ) use ( &$redirection ) {
			$redirection = $location;
			return null;
		} );

		$id4me_client = new ID4me_Client( $service );

		$id4me_client->redirect_to_authority(
			$authority,
			'wikipedia.org',
			'dummy-redirect.com'
		);

		if ( $redirected ) {
			$this->assertNotNull( $redirection );
		} else {
			$this->assertNull( $redirection );
		}
	}

	/**
	 * Data Provider for test_redirect_to_authority()
	 *
	 * @return array
	 */
	public function provider_redirect_to_authority() {

		return array(
			array(
				$this->_get_dummy_wp_authority(),
				'http://dummy-url.com',
				true
			),
			array(
				$this->_get_dummy_wp_authority(),
				null,
				false
			),
			array(
				new ID4me_WP_Authority(),
				'http://dummy-url.com',
				false
			),
			array(
				null,
				'http://dummy-url.com',
				false
			)
		);
	}

	/**
	 * @test ID4me_Client::authorize_user()
	 * @dataProvider provider_authorize_user
	 *
	 * @param string  $authority_hostname
	 * @param array   $access_data
	 * @param boolean $authorized
	 */
	public function test_authorize_user( $authority_hostname, $access_data, $authorized_access_data ) {

		ID4me_WP_Authority::create_table();

		$service = $this->_get_service_mock();

		$service->method( 'createOpenIdConfigFromJson' )
		        ->willReturn( $this->_get_dummy_configuration() );

		$service->method( 'authorize' )
		        ->willReturn( $access_data );

		$existing_authority = $this->_get_dummy_wp_authority();
		$existing_authority->save();

		$id4me_client = new ID4me_Client( $service );

		$this->assertSame(
			$id4me_client->authorize_user(
				$authority_hostname,
				'dummy-code'
			),
			$authorized_access_data
		);

		ID4me_WP_Authority::delete_table();
	}

	/**
	 * Data Provider for test_authorize_user()
	 *
	 * @return array
	 */
	public function provider_authorize_user() {

		return array(
			array(
				'dummy-authority.com',
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234'
				),
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234'
				)
			),
			array(
				'dummy-authority.com',
				array(),
				array()
			),
			array(
				'dummy-not-existing-authority.com',
				array(),
				false
			),
			array(
				null,
				array(),
				false
			),
			array(
				null,
				null,
				false
			)
		);
	}

	/**
	 * @test ID4me_Client::get_param()
	 * @dataProvider provider_get_param
	 *
	 * @param string $param
	 * @param string $value
	 * @param string $final_value
	 */
	public function test_get_param( $param, $value, $final_value ) {

		$_GET[ $param ] = $value;

		$id4me_client = new ID4me_Client(
			new ID4me_WP_Authority()
		);

		$this->assertSame(
			$id4me_client->get_param( $param ),
			$final_value
		);
	}

	/**
	 * Data Provider for test_get_param()
	 *
	 * @return array
	 */
	public function provider_get_param() {

		return array(
			array(
				null,
				null,
				false,
			),
			array(
				'dummy-param',
				'test',
				false,
			),
			array(
				'id4me_identifier',
				'test',
				'test',
			),
			array(
				'id4me_identifier',
				'test',
				'test',
			),
			array(
				'id4me_identifier',
				'https://wikipedia.org',
				'https://wikipedia.org',
			),
			array(
				'id4me_identifier',
				'wikipedia.org',
				'wikipedia.org',
			),
			array(
				'dummy_param',
				'wikipedia.org',
				false,
			),
			array(
				'code',
				'<a href="">test</a>',
				'test',
			),
			array(
				'code',
				"\ntest",
				'test'
			)
		);
	}

	/**
	 * Get Mock Object for Id4me\RP\Service class
	 *
	 * @return \PHPUnit\Framework\MockObject\MockObject
	 */
	private function _get_service_mock() {

		return $this->getMockBuilder( 'Id4me\RP\Service' )
		            ->setMethods( array(
			            'discover',
			            'getOpenIdConfig',
			            'register',
			            'createOpenIdConfigFromJson',
			            'getAuthorizationUrl',
			            'getAccessTokens'
		            ) )
		            ->getMock();
	}

	/**
	 * Get Mock Object for our own ID4me_WP_Authority class
	 *
	 * @return ID4me_WP_Authority
	 */
	private function _get_dummy_wp_authority() {

		$authority = new ID4me_WP_Authority();
		$authority->set_id( 1 );
		$authority->set_hostname( 'dummy-authority.com' );
		$authority->set_client_id( 'abcd123' );
		$authority->set_client_secret( 'qefeq6g47r8ghhet9rht9rh8t6' );
		$authority->set_configuration( '{[]}' );

		return $authority;
	}

	/**
	 * Generate a Client Model object
	 *
	 * @return \Id4me\RP\Model\Client
	 */
	private function _get_dummy_client() {

		$client = new \Id4me\RP\Model\Client( 'dummy-authority.com' );
		$client->setClientId( 'abcd123' );
		$client->setClientSecret( '00000000000000000000000' );
		$client->setClientExpirationTime( 0 );

		return $client;
	}

	/**
	 * Generate an OpenIdConfig Model object
	 *
	 * @return \Id4me\RP\Model\OpenIdConfig
	 */
	private function _get_dummy_configuration() {

		return new \Id4me\RP\Model\OpenIdConfig( array() );
	}
}
