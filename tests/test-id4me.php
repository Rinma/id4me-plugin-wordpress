<?php
require_once './id4me.php';

class Test_ID4me extends WP_UnitTestCase {

	/**
	 * @test id4me_delete_expired_transients()
	 * @dataProvider provider_delete_expired_transients
	 *
	 * @param string $prefix
	 * @param string $identifier_hash_key
	 * @param string $identifier
	 */
	public function test_delete_expired_transients( $prefix, $identifier_hash_key, $identifier ) {

		set_transient( $prefix . $identifier_hash_key, $identifier, 1 );
		sleep( 2 );

		id4me_delete_expired_transients( $prefix );

		$this->assertFalse(
			get_transient( $prefix . $identifier_hash_key )
		);
	}

	/**
	 * @test id4me_cron_init()
	 */
	public function test_cleanup_cron() {

		id4me_cron_init();

		$cron_job_found = false;
		$scheduled_cron_jobs = _get_cron_array();

		foreach ( $scheduled_cron_jobs as $timestamp => $hooks ) {
			foreach ( ( array ) $hooks as $hook => $cron_job ) {
				if ( 'id4me_cron_cleanup' === $hook ) {
					$cron_job_found = true;
				}
			}
		}

		$this->assertTrue( $cron_job_found );
	}

	/**
	 * Data Provider for test_delete_expired_transients()
	 *
	 * @return array
	 */
	public function provider_delete_expired_transients() {

		return array(
			array(
				'id4me',
				uniqid( '', true ),
				'w3schools.com',
			),
			array(
				'id4me',
				'',
				'wikipedia.org',
			),
			array(
				'',
				uniqid( '', true ),
				'stackoverflow.com',
			),
			array(
				'',
				'test',
				'wordpress.org',
			)
		);
	}
}