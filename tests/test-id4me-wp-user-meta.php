<?php
require_once './includes/class-id4me-user.php';

class Test_ID4me_User extends WP_UnitTestCase {

	/**
	 * @test Test_ID4me_WP_User_Meta::get_wp_user()
	 * @dataProvider provider_get_wp_user
	 *
	 * @param string  $identifier
	 * @param string  $user_meta_value
	 * @param boolean $has_user_been_found
	 */
	public function test_get_wp_user( $identifier, $user_meta_value, $has_user_been_found ) {

		$user_id = $this->factory->user->create();
		update_user_meta( $user_id, 'id4me_identifier', $user_meta_value );

		try {
			$user_meta = new ID4me_User( $identifier );
			$user = $user_meta->get_wp_user();

			$found = ( $user instanceof WP_User );

		} catch ( Exception $exception ) {
			$found = false;
		}

		$this->assertSame(
			$has_user_been_found,
			$found
		);
	}

	/**
	 * Data Provider for test_is_connect_action()
	 *
	 * @return array
	 */
	public function provider_get_wp_user() {

		return array(
			array(
				'domain.com',
				'domain.com',
				true,
			),
			array(
				'domain.com',
				null,
				false,
			),
			array(
				'domain.com',
				'some_meta',
				false,
			)
		);
	}
}
